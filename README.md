menggunakan database skilltest
table:
1. usertest
2. shoppingtest

Gunakan URL:
1. localhost/testBTS/public/api/users/signup => untuk menyimpan data user
2. localhost/testBTS/public/api/users/sigin => untuk mendapatkan data user berdasarkan email dan password
3. localhost/testBTS/public/api/users/ => untuk mendapatkan semua data user
4. localhost/testBTS/public/api/shopping/ => untuk mengambil semua data shopping
5. localhost/testBTS/public/api/shopping/{id} => untuk mengambil 1 data shopping dengan id tertentu
6. localhost/testBTS/public/api/shopping/store => untuk menyimpan data shopping
7. localhost/testBTS/public/api/shopping/update/{id} => untuk memperbaharui 1 data shopping
8. localhost/testBTS/public/api/shopping/delete/{id} => untuk menghapus 1 data shopping
