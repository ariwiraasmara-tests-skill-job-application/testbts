<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/users', '\App\Http\Controllers\UsertestController@index');
Route::post('/users/signup', '\App\Http\Controllers\UsertestController@store');
Route::post('/users/signin', '\App\Http\Controllers\UsertestController@signin');

Route::get('/shopping', '\App\Http\Controllers\ShoppingtestController@index');
Route::get('/shopping/{id}', '\App\Http\Controllers\ShoppingtestController@show');
Route::post('/shopping/store', '\App\Http\Controllers\ShoppingtestController@store');
Route::put('/shopping/update/{id}', '\App\Http\Controllers\ShoppingtestController@update');
Route::delete('/shopping/delete/{id}', '\App\Http\Controllers\ShoppingtestController@destroy');
