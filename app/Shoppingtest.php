<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shoppingtest extends Model
{

    protected $table = 'shoppingtest';
    //public $table = 'my101_user_login';
    protected $fillable = ["name", "createdated"];

    public $timestamps = false;
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'updated_date';
}
