<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usertest extends Model
{
    protected $table = 'usertest';
    //public $table = 'my101_user_login';
    protected $fillable = ["username",
                            "password",
                            "email",
                            "phone",
                            "country",
                            "city",
                            "postcode",
                            "name",
                            "address"];

    public $timestamps = false;
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'updated_date';
}
