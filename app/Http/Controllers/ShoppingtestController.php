<?php

namespace App\Http\Controllers;

use App\Shoppingtest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ShoppingtestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $data = Shoppingtest::all();
        return response()->json($data, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ShoppingtestRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
        $data = $request->all();
        Shoppingtest::create($data);
        return response()->json('success', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Shoppingtest  $shoppingtest
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $where = array('id'=>$id);
        $data = Shoppingtest::where($where)->get();
        return response()->json($data, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Shoppingtest  $shoppingtest
     * @return \Illuminate\Http\Response
     */
    public function edit(Shoppingtest $shoppingtest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateShoppingtestRequest  $request
     * @param  \App\Models\Shoppingtest  $shoppingtest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, Shoppingtest $shoppingtest)
    {
        //
        $where = array('id'=> $id);
        $request->except(['_token']);
        $shoppingtest::where($where)->update($request->except(['_token', '_method']));
        return response()->json('update success', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Shoppingtest  $shoppingtest
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Shoppingtest $shoppingtest)
    {
        //
        $where = array('id'=> $id);
        $shoppingtest::where($where)->delete();
        return response()->json('delete success', 200);
    }
}
