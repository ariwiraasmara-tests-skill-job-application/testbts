<?php

namespace App\Http\Controllers;

use App\Usertest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class UsertestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $data = Usertest::all();
        return response()->json($data, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreUsertestRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
        $data = $request->all();
        Usertest::create($data);
        $response = ['success',
                    'username' =>$request->username,
                    'email' =>$request->email,];
        return response()->json($response, 201);
    }

    public function signin(Request $request) {
        $datawhere = ['email' => $request->email,
                      'password' => $request->password];
        $datafinal = Usertest::where($datawhere)->get();
        if(is_null($datafinal) || count($datafinal) == 0) {
            return response()->json(['message' => 'Data not found!'], 404);
        }
        else {
            $response = ['success', 'data'=>$datafinal];
            return response()->json($response, 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Usertest  $usertest
     * @return \Illuminate\Http\Response
     */
    public function show(Usertest $usertest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Usertest  $usertest
     * @return \Illuminate\Http\Response
     */
    public function edit(Usertest $usertest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\Usertest  $usertest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Usertest $usertest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Usertest  $usertest
     * @return \Illuminate\Http\Response
     */
    public function destroy(Usertest $usertest)
    {
        //
    }
}
